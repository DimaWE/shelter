<?php

include "Animal.php";
include "Shelter.php";


$shelter = new Shelter();

$shelter->addAnimal('dog', 'Тузик', 1, '20-05-2018');
$shelter->addAnimal('dog', 'Морда', 5, '01-05-2018');
$shelter->addAnimal('cat', 'Cat1', 6, '03-05-2018');
$shelter->addAnimal('dog', 'Мопсик', 3, '14-05-2018');
$shelter->addAnimal('turtle', 'Turtle1', 3, '20-05-2018');
$shelter->addAnimal('dog', 'Кашка', 4);
$shelter->addAnimal('cat', 'Cat2', 5, '30-05-2018');
$shelter->addAnimal('turtle', 'Turtle2', 6, '11-05-2018');
//$shelter->addAnimal('caw', 'Cat2', 5, '30-05-2018');

print_r($shelter->showByType('dog'));
print_r($shelter->giveByType('dog'));
print_r($shelter->showByType('dog'));