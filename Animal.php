<?php

class Animal 
{   
    /**
	 * Свойство класса
	 *
	 * @var string тип животного
	 */
    private $type;

    /**
	 * Свойство класса
	 *
	 * @var string имя животного
	 */
    private $name;

    /**
	 * Свойство класса
	 *
	 * @var int возраст животного
	 */
    private $age;

    /**
	 * Свойство класса
	 *
	 * @var DateTime дата добавления в приют
	 */
    private $dateReceipt;

    /**
	 * Конструктор. Создает новое животное
	 *
	 * @param string $type тип
     * @param string $name имя
     * @param int $age возраст
     * @param DateTime $dateReceipt дата прибытия в приют
     * 
	 * @return Animal новое животное 
	 */
    function __construct(string $type, string $name, int $age, DateTime $dateReceipt)
    {
        $this->type = $type;
        $this->name = $name;
        $this->age = $age;
        $this->dateReceipt = $dateReceipt;
    }

    /**
	 * Получить тип животного
	 *
	 * @return string тип животного
	 */
    public function getType(): string
    {
        return $this->type;
    }

    /**
	 * Получить имя животного
	 *
	 * @return string имя животного
	 */
    public function getName(): string 
    {
        return $this->name;
    }

    /**
	 * Получить возраст животного
	 *
	 * @return int возраст животного
	 */
    public function getAge(): int
    {
        return $this->age;
    }

    /**
	 * Получить дату добавления животного
	 *
	 * @return string дата добавления животного
	 */
    public function getDateReceipt(): DateTime
    {
        return $this->dateReceipt;
    }
}
