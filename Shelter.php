<?php

/**
 * Класс для работы с приютом
 * 
 * @author Shevtsov Dmitriy <dima90-dima@mail.ru>
 */

class Shelter
{
    /**
	 * Свойство класса
	 *
	 * @var array массив с животными в приюте
	 */
    private $animals = [];

    /**
	 * Свойство класса
	 *
	 * @var array возможные типы животных
	 */
    private $types = ['cat', 'dog', 'turtle'];


    /**
	 * Добавляет животное в приют
	 *
	 * @param string $type тип
     * @param string $name имя
     * @param int $age возраст
     * @param string $dateReceipt дата добавления
     * 
	 * @return void 
     * 
     * @throws Exception
	 */
    public function addAnimal(string $type, string $name, int $age, string $dateReceipt = ''): void
    {
        if(in_array($type, $this->types)) {
            $dateReceipt = $dateReceipt ?? date();
            $this->animals[] = new Animal($type, $name, $age, new DateTime($dateReceipt));
        }
        else
            throw new Exception("Animal type not found");
            
    }

    /**
	 * Получить список животных определенного типа, по алфавиту
	 *
	 * @param string $type тип
     * 
	 * @return Animal[] массив животных 
	 */
    public function showByType(string $type): array
    {
        $animals =  array_filter($this->animals, function(Animal $animal) use ($type) {
            return $animal->getType() === $type;
        });
        usort($animals, function (Animal $a, Animal $b) {
            if ($a->getName() == $b->getName()) {
                return 0;
            }
            return ($a->getName() < $b->getName()) ? -1 : 1;           
        });
        return $animals;
    }

    /**
	 * Животное, которое дольше всех находится в приюте, отдается человеку
	 *
	 * @return Animal животное 
	 */
    public function give(): ?Animal
    {
        if(count($this->animals) > 0) {
            $this->_sortByDateReceipt();

            $animal = $this->animals[0];
            unset($this->animals[0]);
            return $animal; 
        }
        else {
            return null;
        }
    }

    /**
	 * Животное определенного типа, которое дольше всех находится в приюте, отдается человеку
	 *
	 * @param string $type: тип
     * 
	 * @return Animal животное 
	 */
    public function giveByType(string $type): ?Animal
    {
        $this->_sortByDateReceipt();

        foreach($this->animals as $key => $animal) {
            if($animal->getType() === $type) {
                unset($this->animals[$key]);
                return $animal;
            }
        }
        
        return null; 
    }

    /**
	 * Сортировка животных по дате нахождения в приюте
	 *
	 * @return void 
	 */
    private function _sortByDateReceipt(): void
    {
        usort($this->animals, function (Animal $a, Animal $b) {
            if ($a->getDateReceipt() == $b->getDateReceipt()) {
                return 0;
            }
            return ($a->getDateReceipt() < $b->getDateReceipt()) ? -1 : 1;           
        });
    }
}
